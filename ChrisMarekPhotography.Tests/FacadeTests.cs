﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChrisMarekPhotography.Models;
using Xunit;

namespace ChrisMarekPhotography.Tests
{
    //Note to self. Don't test the X-Manager classes. Those are used as dependency-breakers
    //Trying to test them is weird. Its testing the testing. Only test the Domain classes.
    //X-Manager classes such as DirectoriesManager, FileReaderManager, FolderFilesManager, ServerPathManager
    public class FacadeTests
    {
        private IFacade _facade;
        private IDao _dao;

        public FacadeTests()
        {
            _dao = new Dao();
            _facade = new Facade();
        }


        /// <summary>
        /// The method should succeed when a unique item is attemped to be added to a dictionary
        /// </summary>
        [Fact]
        public void AddPhotoToDictionary_UniqueShouldWork()
        {
            Photograph photograph = new Photograph { folder = "bluebird", index = "01a" };
            Dictionary<string, IPhotograph> photographDictionary = new Dictionary<string, IPhotograph>();

            _facade.AddPhotoToDictionary(photograph, photographDictionary);
            Assert.True(photographDictionary.Count == 1);
            Assert.True(photographDictionary.ContainsKey("01a"));
        }

        /// <summary>
        /// The method should fail when a second, non-unique item is attemped to be added to a dictionary
        /// </summary>
        [Fact]
        public void AddPhotoToDictionary_NonUniqueShouldFail()
        {
            //Arrange
            Photograph photograph = new Photograph { folder = "bluebird", index = "01a" };
            Dictionary<string, IPhotograph> photographDictionary = new Dictionary<string, IPhotograph>();
            //Act
            string resultMessage = _facade.AddPhotoToDictionary(photograph, photographDictionary);
            resultMessage = _facade.AddPhotoToDictionary(photograph, photographDictionary);
            //Assert
            Assert.True(resultMessage.IndexOf("already exists in folder") > -1);
        }

        /// <summary>
        /// This method should be able to return the correct data for the file path specified.
        /// For example, 
        ///      a ~indexes.txt path should retrieve data from the indexes.txt
        ///      a ~details.txt path should retrieve data from the details.txt
        /// This method is not validating the data, just retrieving it.
        /// </summary>
        /// <param name="indexes">CSV data for a indexes.txt file</param>
        /// <param name="details">CSV data for a details.txt file</param>
        /// <param name="folder">folder name</param>
        /// <param name="expected">CSV data from indexes.txt</param>
        [Theory]
        [InlineData(new string[] { "1", "2" }, new string[] { "1", "2", "3", "4" }, @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\indexes.txt", new string[] { "1", "2" })]
        [InlineData(new string[] { "1", "2", "3" }, new string[] { "1" }, @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\details.txt", new string[] { "1" })]
        public void GetPhotoFolderData_RetrievesDataFromIndexesOrDetailsFile(string[] indexes, string[] details, string path, string[] expected)
        {
            //Arrange
            StubFileReaderManager fm = new StubFileReaderManager();
            fm.indexes = indexes;
            fm.details = details;

            _dao.SetFileReaderManager(fm);
            _facade.SetDao(_dao);    // Dao injection needed because of above Dao set-ups

            //Act
            string[] result = _facade.GetPhotoFolderData(path);

            //Assert
            Assert.Equal(expected, result);
        }

        /// <summary>
        /// there is a expectation of what the path is for any folder
        /// </summary>
        /// <param name="folder">folder name</param>
        /// <param name="dataFile">CSV file indexes.txt or details.txt</param>
        /// <param name="serverPath">physical path for a folder</param>
        /// <param name="expected">correct format that should be returned</param>
        [Theory]
        [InlineData("acornwoodpecker", "indexes", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\photos\acornwoodpecker\indexes.txt")]
        public void GetPath_ShouldBeEqualToExpected(string folder, string dataFile, string expected)
        {
            //Arrange
            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";
            _facade.SetPathManager(spm);

            //Act
            string result = _facade.GetPath(folder, dataFile);
            //Assert
            Assert.True(expected == result);
        }

        /// <summary>
        /// The GetAllPhotographIndexes is never supposed to retrieve indexes from the slideshow and portfolio folders.
        /// </summary>
        /// <param name="indexes"></param>
        /// <param name="directories"></param>
        /// <param name="serverPath"></param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3", "4", "5", "6" }, new string[] { "acornwoodpecker" }, 6)]
        [InlineData(new string[] { "1", "2", "3" }, new string[] { "slideshow" }, 0)]
        [InlineData(new string[] { "1", "2", "3", "4" }, new string[] { "portfolio" }, 0)]
        public void GetAllPhotographIndexes_PortfolioSlideshowCountAlwaysZero(string[] indexes, string[] directories, int expected)
        {
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.indexes = indexes;

            StubDirectoriesManager sdm = new StubDirectoriesManager();
            sdm.directories = directories;

            StubFileManager sfrm = new StubFileManager();
            sfrm.files = new string[] { "indexes.txt" }; //slideshow or portfolio only have a indexes.txt file in their folder

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";

            _dao.SetFilesManager(sfrm);       //initializes Dao
            _dao.SetFileReaderManager(sfm);   //initializes Dao
            _dao.SetDirectoriesManager(sdm);  //initializes Dao
            _facade.SetPathManager(spm);      //initializes Facade

            _facade.SetDao(_dao);    // Dao injection needed because of above Dao set-ups

            //Act
            int count = _facade.GetAllPhotographIndexes().Count;

            //Assert
            Assert.True(expected == count);
        }

        /// <summary>
        /// The GetAllPhotographIndexes is always supposed to retrieve indexes from the VALIDATED
        /// folders with the exception of slideshow and portfolio folders.
        /// </summary>
        /// <param name="indexes"></param>
        /// <param name="directories"></param>
        /// <param name="serverPath"></param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3", "4", "5" }, new string[] { "1", "2", "3", "4" }, new string[] { "portfolio" }, 0)]
        [InlineData(new string[] { "1", "2", "3", "4", "5" }, new string[] { "1", "2", "3", "4" }, new string[] { "slideshow" }, 0)]
        [InlineData(new string[] { "1", "2", "3", "4", "5" }, new string[] { "1", "2", "3" }, new string[] { "bluebird" }, 0)]
        [InlineData(new string[] { "1", "2", "3", "4", "5" }, new string[] { "1", "2", "3", "4" }, new string[] { "bluebird" }, 5)]
        [InlineData(new string[] { "1", "2", "3", "4" }, new string[] { "1", "2", "3", "4" }, new string[] { "acornwoodpecker" }, 4)]
        public void GetAllPhotographIndexes_ValidfoldersAlwaysReturnIndexes(string[] indexes, string[] details, string[] directories, int expected)
        {
            //Arrange
            StubFileReaderManager fm = new StubFileReaderManager();
            fm.indexes = indexes;
            fm.details = details;

            StubDirectoriesManager sdm = new StubDirectoriesManager();
            sdm.directories = directories;

            StubFileManager sfrm = new StubFileManager();
            sfrm.files = new string[] { "indexes.txt", "details.txt" };

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";


            _dao.SetFilesManager(sfrm);       //initializes Dao
            _dao.SetFileReaderManager(fm);    //initializes Dao
            _dao.SetDirectoriesManager(sdm);  //initializes Dao
            _facade.SetPathManager(spm);      //initializes Facade

            _facade.SetDao(_dao);    // Dao injection needed because of above Dao set-ups

            //Act
            int count = _facade.GetAllPhotographIndexes().Count;

            //Assert
            Assert.True(count == expected);
        }

        /// <summary>
        /// indexes.txt must have greater than 0 parts or it will fail validation and the folder is declared invalid
        /// </summary>
        /// <param name="indexes">CSV data from a indexes.txt file</param>
        /// <param name="directories">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="serverPath">the path of the directory</param>
        /// <param name="fileNames">the files inside a folder</param>
        [Theory]
        [InlineData(new string[] { "1" }, new string[] { "1", "2", "3", "4" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\acornwoodpecker", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\redwingedblackbird" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\indexes.txt" })]
        [InlineData(new string[] { "1", "2", "3", "4" }, new string[] { "1", "2", "3", "4" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\acornwoodpecker", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\redwingedblackbird" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\indexes.txt" })]
        public void GetAllPhotographIndexes_ValidIndexesWhenGreaterThanZero(string[] indexes, string[] details, string[] directories, string[] fileNames)
        {
            //Arrange
            StubFileReaderManager sfrm = new StubFileReaderManager();
            sfrm.indexes = indexes;
            sfrm.details = details;

            StubDirectoriesManager sdm = new StubDirectoriesManager();
            sdm.directories = directories;

            StubFileManager sfm = new StubFileManager();
            sfm.files = fileNames;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";

            _dao.SetFilesManager(sfm);        //initializes Dao
            _dao.SetFileReaderManager(sfrm);  //initializes Dao
            _dao.SetDirectoriesManager(sdm);  //initializes Dao
            _facade.SetPathManager(spm);      //initializes Facade

            _facade.SetDao(_dao);    // Dao injection needed because of above Dao set-ups

            //Act
            int count = _facade.GetAllPhotographIndexes().Count;

            //Assert
            Assert.True(count > 0);
        }
        /// <summary>
        /// Details.txt must have at LEAST 4 parts or it will fail validation and NO indexs are returned because the 
        /// folder that contains that invalid Details.txt is declared invalid and not used by the application
        /// </summary>
        /// <param name="indexes">CSV data from a indexes.txt file</param>
        /// <param name="directories">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="serverPath">the path of the directory</param>
        /// <param name="fileNames">the files inside a folder</param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3", "4", "5", "6" }, new string[] { "1", "2", "3" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\acornwoodpecker", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\redwingedblackbird" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\details.txt" }, 0)]
        [InlineData(new string[] { "1", "2", "3", "4", "5" }, new string[] { "1", "2", "3", "4" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\acornwoodpecker", @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\redwingedblackbird" }, new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird\details.txt" }, 15)]
        public void ValidateFolders_DetailsFileLessThanFourPartsMustFail(string[] indexes, string[] details, string[] directories, string[] fileNames, int expected)
        {
            //Arrange
            StubFileReaderManager sfrm = new StubFileReaderManager();
            sfrm.indexes = indexes;
            sfrm.details = details;

            StubDirectoriesManager sdm = new StubDirectoriesManager();
            sdm.directories = directories;

            StubFileManager sfm = new StubFileManager();
            sfm.files = fileNames;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";

            _dao.SetFilesManager(sfm);       //initializes Dao with stub
            _dao.SetFileReaderManager(sfrm);   //initializes Dao with stub
            _dao.SetDirectoriesManager(sdm);  //initializes Dao with stub
            _facade.SetPathManager(spm);      //initializes Facade with stub

            _facade.SetDao(_dao);    // Dao injection needed because of above Dao set-ups

            //Act
            int count = _facade.GetAllPhotographIndexes().Count;

            //Assert
            Assert.True(expected == count);
        }

        /// <summary>
        ///  CSV data file can only be Indexes.txt or details.txt
        /// </summary>
        /// <param name="indexes"></param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="serverPath">the path of the directory</param>
        /// <param name="fileName">the files inside a folder</param>
        [Theory]
        [InlineData(new string[] { "Here we go gatherring nuts in may" }, new string[] { "1", "2", "3", "4" }, @"bluebird", @"mispelleddetails")]
        public void ValidateFolders_OnlyIndexOrDetailsFileOtherWiseInvalid(string[] indexes, string[] details, string folder, string fileName)
        {
            //Arrange
            StubFileReaderManager sfrm = new StubFileReaderManager();
            sfrm.indexes = indexes;
            sfrm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";

            _dao.SetFileReaderManager(sfrm);
            _facade.SetDao(_dao);
            _facade.SetPathManager(spm);

            //Act
            IPhotosResult photosResult = _facade.ValidatePhotosFolder(folder, fileName);

            //Assert
            Assert.True(photosResult.HasErrors == true);
        }

        /// <summary>
        /// if no indexes are found in the Indexes.txt file, then that will be considerred as an error
        /// </summary>
        /// <param name="indexes">CSV data from a indexes.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { }, new string[] { "1", "2", "3", "4" }, "bluebird", "indexes")]
        public void ValidatePhotosFolder_ZeroIndexesShouldBeInValid(string[] indexes, string[] details, string folder, string fileType)
        {
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.indexes = indexes;
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";

            _facade.SetPathManager(spm);
            _dao.SetFileReaderManager(sfm);
            _facade.SetDao(_dao);

            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);
            //Assert
            Assert.True(validationResult.HasErrors == true);
        }
        /// <summary>
        /// if the CSV data is greater than 0 in the Indexes.txt file, then that will be considerred as a valid indexes.txt file
        /// </summary>
        /// <param name="indexes">CSV data from a indexes.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3" }, new string[] { "1", "2", "3", "4" }, "bluebird", "indexes")]
        public void ValidatePhotosFolder_IndexesShouldBeValid(string[] indexes, string[] details, string folder, string fileType)
        {
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.indexes = indexes;
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";

            _facade.SetPathManager(spm);
            _dao.SetFileReaderManager(sfm);
            _facade.SetDao(_dao);

            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);
            //Assert
            Assert.True(validationResult.HasErrors == false);
        }

        /// <summary>
        /// if no CSV data are found in the details.txt file, then that will be considerred as an error
        /// </summary>
        /// <param name="details">CSV data from a indexes.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { }, "bluebird", "details")]
        public void ValidatePhotosFolder_ZeroDetailsShouldBeInValid(string[] details, string folder, string fileType)
        {
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";

            _facade.SetPathManager(spm);
            _dao.SetFileReaderManager(sfm);
            _facade.SetDao(_dao);

            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);

            //Assert
            Assert.True(validationResult.HasErrors == true, "The details should have 4 parts");
        }
        /// <summary>
        /// if details.txt has less than 4 CSV data, then that will be considerred as an error
        /// </summary>
        /// <param name="details">CSV data from a details.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3" }, "bluebird", "details")]
        public void ValidatePhotosFolder_DetailsShouldBeInValid(string[] details, string folder, string fileType)
        {
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";

            _facade.SetPathManager(spm);
            _dao.SetFileReaderManager(sfm);
            _facade.SetDao(_dao);

            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);

            //Assert
            Assert.True(validationResult.HasErrors == true, "The details should have at least 4 parts");
        }

        /// <summary>
        /// if details.txt has 4 CSV data (or more), then that will be considerred as a valid details.txt file
        /// </summary>
        /// <param name="details">CSV data from a details.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { "1", "2", "3", "4" }, "bluebird", "details")]
        public void ValidatePhotosFolder_DetailsShouldBeValid(string[] details, string folder, string fileType)
        {
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";

            _facade.SetPathManager(spm);
            _dao.SetFileReaderManager(sfm);
            _facade.SetDao(_dao);

            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);

            //Assert
            Assert.True(validationResult.HasErrors == false, "4 parts to the details should not throw an error");
        }
        /// <summary>
        /// if csv data in indexes.txt is not numeric, the file folder containing that file is invalid
        /// </summary>
        /// <param name="details">CSV data from a details.txt file</param>
        /// <param name="folder">the folder containing the details.txt file and the indexes.txt file</param>
        /// <param name="fileType">details.txt or indexes.txt</param>
        [Theory]
        [InlineData(new string[] { "1", "2A", "3", "4" }, "bluebird", "indexes")]
        public void ValidatePhotosFolder_IndexesValuesMustBeNumeric(string[] details, string folder, string fileType)
        {
            //Arrange
            StubFileReaderManager sfm = new StubFileReaderManager();
            sfm.details = details;

            StubServerPathManager spm = new StubServerPathManager();
            spm.serverPath = @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography";

            _facade.SetPathManager(spm);
            _dao.SetFileReaderManager(sfm);
            _facade.SetDao(_dao);


            //Act
            IPhotosResult validationResult = _facade.ValidatePhotosFolder(folder, fileType);

            //Assert
            Assert.True(validationResult.HasErrors == true, "data in the indexes.txt file must be numeric");
        }
    }

    //*********************************************************************************************************************************************************************************
    //*******************************************************************           STUBS           ***********************************************************************************
    //*********************************************************************************************************************************************************************************
    /// <summary>
    /// This method is used to read comma separated text files (details.txt and indexes.txt)
    /// and return a string array of the comma separated data
    /// </summary>
    public class StubFileReaderManager : IFileReaderManager
    {
        public string[] indexes { get; set; }
        public string[] details { get; set; }
        public string[] FileReader(string path)
        {
            string[] arrayResult = (parseName(path) == "indexes") ? indexes : details;
            return arrayResult;
        }

        public string parseName(string path)
        {
            int lastIndex = path.LastIndexOf(@"\");
            string fileName = path.Substring(lastIndex + 1);
            fileName = fileName.Split('.')[0];
            return fileName;
        }
    }

    /// <summary>
    /// This method is used to find out the directories (their names)
    /// return a string array of folder names
    /// </summary>
    public class StubDirectoriesManager : IDirectoriesManager
    {
        public string[] directories { get; set; }
        public string[] GetAllPhotoFolders()
        {
            return directories;
        }
    }

    /// <summary>
    /// This method is used to find out the files (their names)
    /// return a string array of file names
    /// </summary>
    public class StubFileManager : IFolderFilesManager
    {
        public string[] files { get; set; }
        public string[] GetPhotoFolderFiles(string folder)
        {
            return files;
        }
    }

    /// <summary>
    /// This method figures out the physical file path on the web server
    /// </summary>
    public class StubServerPathManager : IPathManager
    {
        public string serverPath { get; set; }
        public string GetPath()
        {
            return serverPath;
        }
    }
}
