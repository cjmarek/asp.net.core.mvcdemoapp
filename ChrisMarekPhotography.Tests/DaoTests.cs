﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using ChrisMarekPhotography.Common;
using ChrisMarekPhotography.ExtractedDependencies;
using Microsoft.AspNetCore.Hosting;

namespace ChrisMarekPhotography.Tests
{
    public class DaoTests
    {
        IDao _dao;
        public DaoTests()
        {
            _dao = new Dao();
        }

        [Theory]
        [InlineData(@"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\egret\indexes.txt", new string[] { "1", "2", "3" }, new string[] { "1", "2", "3", "4" }, new string[] { "1", "2", "3" })]
        public void GetArray_ShouldBeEqualToExpected(string path, string[] indexes, string[] details, string[] expected)
        {
            //Arrange
            StubFileReaderManager fm = new StubFileReaderManager();
            fm.indexes = indexes;
            fm.details = details;

            _dao.SetFileReaderManager(fm);

            //Act
            var actual = _dao.GetArray(path);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(new string[] { @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\bluebird",
            @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\cactus",
            @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\butterfly",
            @"C:\BitBucketDemoMvcTest\asp.net.mvcdemoapp\ChrisMarekPhotography\egret" },
            new string[] { "bluebird", "cactus", "butterfly", "egret" })]
        public void GetAllPhotoFolders_ShouldBeEqualToExpected(string[] directories, string[] expected)
        {
            //Arrange
            StubDirectoriesManager dm = new StubDirectoriesManager();
            dm.directories = directories;

            _dao.SetDirectoriesManager(dm);

            //Act
            var actual = _dao.GetAllPhotoFolders().ToArray();
            string[] result = actual.Where(act => expected.Search(exp => exp == act)).ToArray();

            //Assert
            Assert.Equal(4, result.Count());
            for (int i = 0; i < 4; i++)
            {
                Assert.Equal(expected[i], actual[i]);
            }
        }
    }
}
