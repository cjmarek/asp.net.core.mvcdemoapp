﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLibrary.DataAccess
{
    internal class SqlDataAccess : IDisposable
    {
        private bool disposedValue;

        public  string GetConnectionString(string connectionName = "MVCDemoDB")
        {
            //try
            //{
            //    return ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            //}
            //catch(Exception e)
            //{
            //    var showme = e.Message;
            //}
            return "";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">This is your data model</typeparam>
        /// <param name="sql">This can be a Stored proc, or maybe inline sql</param>
        /// <returns></returns>
        public  List<T> LoadData<T>(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                return cnn.Query<T>(sql).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">This is your data model</typeparam>
        /// <param name="sql">This can be a Stored proc, or maybe inline sql</param>
        /// <returns></returns>
        public  int SaveData<T>(string sql, T data)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                return cnn.Execute(sql, data);
            }
        }

        public  void UpdateData<T>(T person, string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                cnn.Execute(sql, person);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~SqlDataAccess()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
