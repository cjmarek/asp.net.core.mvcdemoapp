﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLibrary.DataAccess;
using DataLibrary.Models;

namespace DataLibrary.BusinessLogic
{
    public class CustomerProcessor
    {
        //SqlDataAccess _sqlDataAccess;
        public CustomerProcessor() 
        {
           //_sqlDataAccess = new SqlDataAccess();
        }

        public  int CreateCustomer(string firstName,
            string lastName, string emailAddress)
        {
            SqlDataAccess sqlDataAccess = new SqlDataAccess();
            CustomerModel data = new CustomerModel
            {
                FirstName = firstName,
                LastName = lastName,
                EmailAddress = emailAddress
            };

            string sql = @"insert into dbo.Customer (FirstName, LastName, EmailAddress)
                           values (@FirstName, @LastName, @EmailAddress);";

            //return sqlDataAccess.SaveData(sql, data);
            return NoDatabaseYet(sql, data);
        }

        public  List<CustomerModel> LoadCustomers()
        {
            SqlDataAccess sqlDataAccess = new SqlDataAccess();
            string sql = @"select Id, FirstName, LastName, EmailAddress
                           from dbo.Customer;";

            //return sqlDataAccess.LoadData<CustomerModel>(sql);
            return NoDatabaseYet(sql);
        }

        public  int NoDatabaseYet(string sql, CustomerModel data)
        {
            SqlDataAccess sqlDataAccess = new SqlDataAccess();
            int defaultReturn = 0;
            try
            {
                defaultReturn = sqlDataAccess.SaveData(sql, data);
            }
            catch (Exception e)
            {
                var showme = e.Message;
            }
            return defaultReturn;
        }

        public  List<CustomerModel> NoDatabaseYet(string sql)
        {
            SqlDataAccess sqlDataAccess = new SqlDataAccess();
            List<CustomerModel> defaultCustomer = null;
            try
            {
                defaultCustomer = sqlDataAccess.LoadData<CustomerModel>(sql);
            }
            catch (Exception e)
            {
                var showme = e.Message;
            }
            return defaultCustomer;
        }
    }
}
