﻿using DataLibrary.Models;
using System.Collections.Generic;

namespace DataLibrary.BusinessLogic
{
    public interface ICustomerProcessor
    {
        int CreateCustomer(string firstName, string lastName, string emailAddress);
        List<CustomerModel> LoadCustomers();
        List<CustomerModel> NoDatabaseYet(string sql);
        int NoDatabaseYet(string sql, CustomerModel data);
    }
}