﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Hosting;
using ChrisMarekPhotography;

namespace ChrisMarekPhotography.ExtractedDependencies
{
    /// <summary>
    /// This class is used for dependancy-breaking so that the application
    /// is testable
    /// </summary>
    public class ServerPathManager : IPathManager
    {
        private IWebHostEnvironment _host;
        public ServerPathManager(IWebHostEnvironment host)
        {
            _host = host;
        }
        public string GetPath()
        {
            return _host.WebRootPath;
        }
    }
}