﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChrisMarekPhotography.ExtractedDependencies
{
    /// <summary>
    /// This class is used for dependancy-breaking so that the application
    /// is testable
    /// </summary>
    public class FileReaderManager : IFileReaderManager
    {
        public string[] FileReader(string path)
        {
            Utilities.FileReader fr = new Utilities.FileReader(path);
            string[] array = fr.Read();
            fr.Close();
            return array;
        }
    }
}