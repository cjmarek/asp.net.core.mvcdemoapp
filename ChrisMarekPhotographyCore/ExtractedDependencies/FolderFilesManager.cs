﻿using ChrisMarekPhotography;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChrisMarekPhotography.ExtractedDependencies
{
    /// <summary>
    /// This class is used for dependancy-breaking so that the application
    /// is testable
    /// </summary>
    public class FolderFilesManager : IFolderFilesManager
    {
        IWebHostEnvironment _host = null;
        public FolderFilesManager(IWebHostEnvironment host)
        {
            _host = host;
        }
        public string[] GetPhotoFolderFiles(string folder)
        {
            string webRootPath = _host.WebRootPath;
            string[] files = System.IO.Directory.GetFiles(webRootPath + @"\photos\" + folder);

            return files;
        }
    }
}