﻿using ChrisMarekPhotography;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChrisMarekPhotography.ExtractedDependencies
{
    /// <summary>
    /// This class is used for dependancy-breaking so that the application
    /// is testable
    /// </summary>
    public class DirectoriesManager : IDirectoriesManager
    {
        IWebHostEnvironment _host = null;
        public DirectoriesManager(IWebHostEnvironment host)
        {
            _host = host;
        }
        public string[] GetAllPhotoFolders()
        {
            string webRootPath = _host.WebRootPath;

            //string showme = webRootPath + @"\photos";
            string[] directories = System.IO.Directory.GetDirectories(webRootPath + @"\photos");
            return directories;
        }
    }
}