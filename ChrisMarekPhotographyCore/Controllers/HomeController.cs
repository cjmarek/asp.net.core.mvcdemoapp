﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ChrisMarekPhotography.Models;
using DataLibrary.BusinessLogic;

namespace ChrisMarekPhotography.Controllers
{
    /// <summary>
    /// These results will automatically show up in the RenderBody of the _Layout.cshtml
    /// </summary>
    public class HomeController : Controller
    {
        public readonly ILogger<HomeController> _logger;
        public readonly CustomerProcessor _customerProcessor;

        public HomeController(ILogger<HomeController> logger, CustomerProcessor customerProcessor)
        {
            _logger = logger;
            _customerProcessor = customerProcessor;
        }


        /// <summary>
        /// this is the portfolio view
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// this is the portfolio view
        /// Notice the need for ModelState.Clear(); here.
        /// If you remain on the same view after a postBack the MVC assumes
        /// you are fixing input errors and retains the last input values (somehow).
        /// So you need to clear the values.
        /// This issue does not happen in my full .NET Frame work version of this program!
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Models.CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = _customerProcessor.CreateCustomer(
                    model.FirstName,
                    model.LastName,
                    model.EmailAddress);
                ModelState.Clear();
                // return RedirectToAction("Index");  comment out to stay on current view
            }
            return View();
        }


        /// <summary>
        /// this is the slide show view
        /// </summary>
        /// <returns></returns>
        public ActionResult Home()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        /// <summary>
        /// this is the slide show view
        /// Notice the need for ModelState.Clear(); here.
        /// If you remain on the same view after a postBack the MVC assumes
        /// you are fixing input errors and retains the last input values (somehow).
        /// So you need to clear the values.
        /// This issue does not happen in my full .NET Frame work version of this program!
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Home(Models.CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = _customerProcessor.CreateCustomer(
                    model.FirstName,
                    model.LastName,
                    model.EmailAddress);
                ModelState.Clear();
                // return RedirectToAction("Index");  comment out to stay on current view
            }

            return View();
        }

        /// <summary>
        /// this is the drop down selection view
        /// </summary>
        /// <returns></returns>
        public ActionResult Images()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        /// <summary>
        /// this is the drop down selection view
        /// Notice the need for ModelState.Clear(); here.
        /// If you remain on the same view after a postBack the MVC assumes
        /// you are fixing input errors and retains the last input values (somehow).
        /// So you need to clear the values.
        /// This issue does not happen in my full .NET Frame work version of this program!
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Images(Models.CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = _customerProcessor.CreateCustomer(
                    model.FirstName,
                    model.LastName,
                    model.EmailAddress);
                ModelState.Clear();
                // return RedirectToAction("Index");  comment out to stay on current view
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


        /// <summary>
        /// this is the about view
        /// Notice the need for ModelState.Clear(); here.
        /// If you remain on the same view after a postBack the MVC assumes
        /// you are fixing input errors and retains the last input values (somehow).
        /// So you need to clear the values.
        /// This issue does not happen in my full .NET Frame work version of this program!
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult about(Models.CustomerModel model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = _customerProcessor.CreateCustomer(
                    model.FirstName,
                    model.LastName,
                    model.EmailAddress);
                ModelState.Clear();
                // return RedirectToAction("Index");  comment out to stay on current view
            }

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
