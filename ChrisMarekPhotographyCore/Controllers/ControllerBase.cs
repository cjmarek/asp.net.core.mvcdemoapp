﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Hosting;

namespace ChrisMarekPhotography.Controllers
{
    public class ControllerBase : Controller
    {

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMemoryCache _cache;
        private readonly IWebHostEnvironment _host;
        private IFacade _facade;
        private readonly IDao _dao;
        private readonly IPathManager _serverPathManager;

        public class SessionKeys
        {
            public static readonly string ApplicationRoleKey = "__ApplicationRole";
            public static readonly string ApplicationCacheKey = "__ApplicationCache";
            public static readonly string PortfolioKey = "__Portfolio";
            public static readonly string ImagesKey = "__Images";
        }


        public ControllerBase(IHttpContextAccessor httpContextAccessor, IMemoryCache cache, IWebHostEnvironment host, IDao dao, IPathManager serverPathManager)
        {
            _httpContextAccessor = httpContextAccessor;
            _cache = cache;
            _host = host;
            _dao = dao;
            _serverPathManager = serverPathManager;
        }
        /// <summary>
        /// Factory method
        /// </summary>
        /// <returns></returns>
        protected IFacade GetFacade()
        {
            return _facade
                ?? new Facade(_cache, _httpContextAccessor, _host, _dao, _serverPathManager);
        }

        public void SetFacade(IFacade facade)
        {
            _facade = facade;
        }

        public virtual void SetCache()
        {
            string caching = HttpContext.Session.GetString(SessionKeys.ApplicationCacheKey);

            if (caching == null || caching == "false")
            {
                HttpContext.Session.SetString(SessionKeys.ApplicationCacheKey, "true");
            }
            else
            {
                HttpContext.Session.SetString(SessionKeys.ApplicationCacheKey, "false");
                GetFacade().ClearCachedItems();
            }
        }
    }
}
