﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Hosting;

namespace ChrisMarekPhotography.Controllers
{

    public class AjaxController : ControllerBase
    {
        public AjaxController(IHttpContextAccessor httpContextAccessor, IMemoryCache cache, IWebHostEnvironment host, IDao dao, IPathManager serverPathManager) : base(httpContextAccessor, cache, host, dao, serverPathManager)
        {
        
        }
        /// <summary>
        /// Redirect that remains on the same page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ToogleCaching()
        {
            SetCache();
            ActionResult result = Redirect(Request.Headers["Referer"].ToString());
            return result;
        }

        /// <summary>
        /// This is the Portfolio View
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Ajax
        [HttpGet]
        public ActionResult GetPortfolioImages(string folder)
        {
            return ViewComponent("Image", new { folder });
        }

        /// <summary>
        /// This is the SlideShow that is the very first page to be displayed at start up.
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        // GET: Ajax
        [HttpGet]
        public ActionResult GetSlideShowImages(string folder)
        {
            return ViewComponent("SlideShowImage", new { folder });
        }


        /// <summary>
        /// This is the Images that are requested from the drop-down selection on the Images view
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        // GET: Ajax
        [HttpGet]
        public ActionResult SearchImages(string folder)
        {
            return ViewComponent("Image", new { folder });
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
