﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace ChrisMarekPhotography.Models
{
    public class PhotoViewModel : IPhotoViewModel
    {
        public PhotoViewModel(List<IPhotoInformation> photoInfo)
        {
            PhotoInformation = photoInfo;
        }

        public List<IPhotoInformation> PhotoInformation { get; set; }
    }
}