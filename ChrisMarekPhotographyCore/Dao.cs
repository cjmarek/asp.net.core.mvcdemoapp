﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;

namespace ChrisMarekPhotography
{
    public class Dao : IDao
    {
        IFileReaderManager _fileReaderManager;
        IDirectoriesManager _directoriesManager;
        IFolderFilesManager _folderFilesManager;

        public Dao()
        {

        }

        public Dao(IFileReaderManager fileReaderManager, IDirectoriesManager directoriesManager, IFolderFilesManager folderFilesManager)
        {
            _fileReaderManager = fileReaderManager;
            _directoriesManager = directoriesManager;
            _folderFilesManager = folderFilesManager;
        }

        /// <summary>
        /// setter methods for unit testing. Inject stubs 
        /// </summary>
        /// <param name="cache"></param>
        public void SetFileReaderManager(IFileReaderManager fileReaderManager)
        {
            _fileReaderManager = fileReaderManager;
        }
        public void SetDirectoriesManager(IDirectoriesManager directoriesManager)
        {
            _directoriesManager = directoriesManager;
        }

        public void SetFilesManager(IFolderFilesManager folderFilesManager)
        {
            _folderFilesManager = folderFilesManager;
        }

        /// <summary>
        /// This method is used to read comma separated text files (details.txt and indexes.txt)
        /// returns a string array from a comma separated text file
        /// </summary>
        public string[] GetArray(string path)
        {
            string[] array = _fileReaderManager.FileReader(path);
            return array;
        }

        /// <summary>
        /// returns directory names (folder names)
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetAllPhotoFolders()
        {
            int lastIndex = 0;
            string folderName = string.Empty;

            string[] directories = _directoriesManager.GetAllPhotoFolders();
            foreach (string folderPath in directories)
            {
                lastIndex = folderPath.LastIndexOf(@"\");
                folderName = folderPath.Substring(lastIndex + 1);
                yield return folderName;
            }
        }

        /// <summary>
        /// returns file names (index.txt And details.txt files)
        /// from the folder.
        /// Note: portfolio and slideshow folders will only contain an index.txt file.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetPhotoFolderFiles(string folder)
        {
            int lastIndex = 0;
            string fileName = string.Empty;

            foreach (string file in _folderFilesManager.GetPhotoFolderFiles(folder))
            {
                lastIndex = file.LastIndexOf(@"\");
                fileName = file.Substring(lastIndex + 1);
                fileName = fileName.Split('.')[0];
                yield return fileName;
            }
        }
    }
}