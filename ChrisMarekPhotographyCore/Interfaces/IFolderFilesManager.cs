﻿namespace ChrisMarekPhotography
{
    public interface IFolderFilesManager
    {
        string[] GetPhotoFolderFiles(string folder);
    }
}