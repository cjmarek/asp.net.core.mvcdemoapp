﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChrisMarekPhotography
{
    public interface IDao
    {
        IEnumerable<string> GetAllPhotoFolders();
        string[] GetArray(string path);
        void SetDirectoriesManager(IDirectoriesManager directoriesManager);
        void SetFileReaderManager(IFileReaderManager fileReaderManager);
        void SetFilesManager(IFolderFilesManager folderFilesManager);
        IEnumerable<string> GetPhotoFolderFiles(string folder);
    }
}
