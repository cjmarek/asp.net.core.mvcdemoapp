﻿namespace ChrisMarekPhotography
{
    public interface IPathManager
    {
        string GetPath();
    }
}