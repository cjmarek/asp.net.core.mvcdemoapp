﻿using Microsoft.AspNetCore.Hosting;

namespace ChrisMarekPhotography
{
    public interface IServerPath
    {
        IWebHostEnvironment ServerRootPath { get; set; }
    }
}