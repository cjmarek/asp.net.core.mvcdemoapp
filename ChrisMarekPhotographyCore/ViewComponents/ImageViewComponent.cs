﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Hosting;

namespace ChrisMarekPhotography.ViewComponents
{
    public class ImageViewComponent : ViewComponent
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMemoryCache _cache;
        private readonly IWebHostEnvironment _host;
        private readonly IFacade _facade;
        private readonly IDao _dao;
        private readonly IPathManager _serverPathManager;

        public ImageViewComponent(IHttpContextAccessor httpContextAccessor, IMemoryCache cache, IWebHostEnvironment host, IDao dao, IFacade facade, IPathManager serverPathManager)
        {
            _httpContextAccessor = httpContextAccessor;
            _cache = cache;
            _host = host;
            _dao = dao;
            _facade = facade;
            _serverPathManager = serverPathManager;
        }
        public async Task<IViewComponentResult> InvokeAsync(string folder)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            IPhotoViewModel photoViewModel = await Task.Run(() => _facade.GetImages(folder));
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;

            return View("Image", photoViewModel);
        }
    }
}
