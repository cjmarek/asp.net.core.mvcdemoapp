﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace ChrisMarekPhotography.ViewComponents
{
    public class UrlMapViewComponent : ViewComponent
    {

        public UrlMapViewComponent()
        {

        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.Run(()=> View("UrlMap"));
        }
    }
}
