﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace ChrisMarekPhotography.ViewComponents
{
    public class SignUpViewComponent : ViewComponent
    {

        public SignUpViewComponent()
        {

        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            Models.CustomerModel model = await Task.Run(()=> new Models.CustomerModel());
            return View("SignUp", model);
        }
    }
}
