using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace ChrisMarekPhotography
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
		//C# Logging with Serilog and Seq - Structured Logging Made Easy
                var configuration = new ConfigurationBuilder().AddJsonFile(path: "appsettings.json", optional: false, reloadOnChange: true).Build();
                Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Debug()
                            .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                            .Enrich.FromLogContext()
                            .WriteTo.File(configuration.GetValue<string>("connectionSettings:logpath"),
                                          outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message}{NewLine}{Exception}",
                                          fileSizeLimitBytes: 10000000,
                                          rollOnFileSizeLimit: true,
                                          shared: true,
                                          retainedFileCountLimit: 10).CreateLogger();
                Log.Information("Application starting up");
                CreateHostBuilder(args).ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddConfiguration(configuration.GetSection("connectionSettings")); 
                }).Build().Run();
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Application terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog() //use serilog not the default logger from Core
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
