﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ChrisMarekPhotography.Utilities
{

    public class FileReader
    {
        private StreamReader sr;
        private bool eof = false;

        public FileReader(string FilePath)
        {
            try
            {
                sr = new StreamReader(FilePath);
            }
            catch (Exception exc)
            {
                string msg = String.Format("Can't open the file: {0}{1}{2}{3}{4}", Environment.NewLine, FilePath, Environment.NewLine, "Is the file in use?", Environment.NewLine, exc.Message, Environment.NewLine);
                throw new IOException(msg);
            }
        }
        public FileReader(Stream s)
        {
            try
            {
                sr = new StreamReader(s);
            }
            catch (Exception exc)
            {
                string msg = String.Format("Can't open the file stream{0}{1}{2}", Environment.NewLine, "Is the file in use?", Environment.NewLine, exc.Message, Environment.NewLine);
                throw new IOException(msg);
            }
        }
        public string[] Read()
        {
            try
            {
                if (eof)
                    throw new EndOfStreamException("End of file has been reached");
                string line = sr.ReadLine();
                if (sr.Peek() == -1)
                    eof = true;
                return line.Split(new char[] { ',' });
            }
            catch (Exception exc)
            {
                string msg = String.Format("Can't read from file{0}{1}{2}", Environment.NewLine, "Is the file in use?", Environment.NewLine, exc.Message, Environment.NewLine);
                throw new IOException(msg);
            }
        }
        public void Close()
        {
            sr.Close();
        }
        public bool Eof
        {
            get { return eof; }
        }
    }
}