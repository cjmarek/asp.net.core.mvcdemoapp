using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ChrisMarekPhotography.ExtractedDependencies;
using DataLibrary.BusinessLogic;
using ChrisMarekPhotography.Models;
using Serilog;

namespace ChrisMarekPhotography
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Transient objects are always different.The transient OperationId value is different in the IndexModel and in the middleware.
            // Scoped objects are the same for each request but different across each request.
            // Singleton objects are the same for every request.
            // In dependency injection terminology, a service:
            // 1)Is typically an object that provides a service to other objects, such as the IMyDependency service.
            // 2)Is not related to a web service, although the service may use a web service.
            services.AddRazorPages().AddRazorRuntimeCompilation();
            services.AddDistributedMemoryCache();

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false; // consent required
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(20);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            //IHttpContextAccessor is no longer wired up by default, you have to register it yourself
            //https://stackoverflow.com/questions/37371264/invalidoperationexception-unable-to-resolve-service-for-type-microsoft-aspnetc
            services.AddHttpContextAccessor();
            services.AddSingleton<CustomerProcessor>();
            //this was automatically supplied because we chose a MVC project template. (instead of Razor which has no controllers)
            services.AddControllersWithViews();
            services.AddScoped<IDao, Dao>();
            services.AddScoped<IFacade, Facade>();
            services.AddScoped<IFileReaderManager, FileReaderManager>();
            services.AddScoped<IDirectoriesManager, DirectoriesManager>();
            services.AddScoped<IPathManager, ServerPathManager>();
            services.AddScoped<IFolderFilesManager, FolderFilesManager>();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        // The 'app.whatever' where 'whatever' is the middlewear.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSerilogRequestLogging();
            app.UseRouting();

            app.UseAuthorization();

            app.UseSession();
            //Now you can keep endpoints or you can use the routing without the endpoint.  
            //this was automatically supplied because we chose a MVC project template.(instead of Razor)
            //this used to be only a part of MVC requirement in MVC 1 and 2 Core. But now, it is a separate piece of middlewear to make routing available to 
            //any other middlewear and not just MVC.
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Home}/{id?}");
            });
        }
    }
}
